<?php

    /**
     * Plugin Name: WP Admin Dark
     * Plugin Author: Cédrik Dubois
     * Version: 0.1
     */

    function wad_apply_enqueue_theme_files() {
        wp_enqueue_style( "wp-admin-dark", plugins_url( "wp-admin-dark.css", __FILE__ ) );
    }

    add_action( "admin_enqueue_scripts", "wad_apply_enqueue_theme_files" );

?>
